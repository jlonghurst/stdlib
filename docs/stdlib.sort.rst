stdlib.sort package
===================

Submodules
----------

stdlib.sort.heap module
-----------------------

.. automodule:: stdlib.sort.heap
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.sort.insertionx module
-----------------------------

.. automodule:: stdlib.sort.insertionx
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.sort.merge module
------------------------

.. automodule:: stdlib.sort.merge
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.sort.mergex module
-------------------------

.. automodule:: stdlib.sort.mergex
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.sort.quick3way module
----------------------------

.. automodule:: stdlib.sort.quick3way
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.sort.quickx module
-------------------------

.. automodule:: stdlib.sort.quickx
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: stdlib.sort
    :members:
    :undoc-members:
    :show-inheritance:
