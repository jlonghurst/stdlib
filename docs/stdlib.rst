stdlib package
==============

Subpackages
-----------

.. toctree::

    stdlib.abstract
    stdlib.ds
    stdlib.excepts
    stdlib.search
    stdlib.sort
    stdlib.utils

Submodules
----------

stdlib.collections module
-------------------------

.. automodule:: stdlib.collections
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.complex module
---------------------

.. automodule:: stdlib.complex
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.hexdump module
---------------------

.. automodule:: stdlib.hexdump
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.matrices module
----------------------

.. automodule:: stdlib.matrices
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.singleton module
-----------------------

.. automodule:: stdlib.singleton
    :members:
    :undoc-members:
    :show-inheritance:

stdlib.stats module
-------------------

.. automodule:: stdlib.stats
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: stdlib
    :members:
    :undoc-members:
    :show-inheritance:
