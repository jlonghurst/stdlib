stdlib.search package
=====================

Submodules
----------

stdlib.search.binarysearch module
---------------------------------

.. automodule:: stdlib.search.binarysearch
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: stdlib.search
    :members:
    :undoc-members:
    :show-inheritance:
