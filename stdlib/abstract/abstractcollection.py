"""
abstractcollection.py
=========================================================
The abstractcollection.py module for python stdlib
"""

from abc import abstractmethod
from stdlib.abstract.iterator import Iterator
from typing import List

from stdlib.abstract.collection import Collection


class AbstractCollection(Collection):
    """
    An abstract implementation of the abstracted Collection class
    """
    @abstractmethod
    def __iter__(self) -> Iterator:
        ...

    @abstractmethod
    def __sizeof__(self) -> int:
        """
        Return the size of the collection
        :return: collection size
        :rtype: int
        """
        ...

    def __empty__(self) -> bool:
        """
        Checks if the collection is empty
        :return: if collection is empty
        :rtype: bool
        """
        return self.__sizeof__() == 0

    def __contains__(self, item: object) -> bool:
        """
        Checks if the collection contains the object
        :param item: the item to check for
        :type item: object
        :return: if collection contains the object
        :rtype: bool
        """
        it = self.__iter__()
        if item is None:
            while it.__has_next__():
                try:
                    if it.__next__() is None:
                        return True
                except StopIteration:
                    break
        else:
            while it.__has_next__():
                try:
                    if item.__eq__(it.__next__()):
                        return True
                except StopIteration:
                    break

        return False

    def to_list(self) -> List:
        """
        Attempts to create a list from the collection
        :return:
        :rtype:
        """
        raise NotImplementedError
        # r = [object for i in range(self.__sizeof__())]
        # it = self.__iter__()
        # for i in range(len(r)):

    def add(self, e: object) -> bool:
        """
        Attempts to add the object to the collection
        :param e: the item to be added
        :type e: object
        :return: if it was successful in adding the object
        :rtype: bool
        """
        raise NotImplementedError

    def __add__(self, other: object) -> bool:
        return self.add(other)

    def remove(self, o: object) -> bool:
        """
        Attempts to remove the object from the collection
        :param o: the object to try removing
        :type o: object
        :return: the removed object
        :rtype: bool
        """
        it = self.__iter__()
        if o is None:
            while it.__has_next__():
                try:
                    if it.__next__() is None:
                        it.__remove__()
                        return True
                except StopIteration:
                    break
        else:
            while it.__has_next__():
                try:
                    if o.__eq__(it.__next__()):
                        it.__remove__()
                        return True
                except StopIteration:
                    break
        return False

    def contains_all(self, c: List or Collection) -> bool:
        """
        Checks if the collection contains all of another collection
        :param c: the collection to check
        :type c: List | Collection
        :return: if c is contained in the collection
        :rtype: bool
        """
        for e in c:
            if not self.__contains__(e):
                return False

        return True

    def add_all(self, c: List or Collection) -> bool:
        """
        Attempts to add all the items in c to the collection
        :param c: the collection to add
        :type c: List | Collection
        :return: if c was successfully added to the collection
        :rtype: bool
        """
        modified = False
        for e in c:
            if self.add(e):
                modified = True

        return modified

    def clear(self):
        pass
