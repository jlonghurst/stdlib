"""
bag.py
=========================================================
The bag.py module for python stdlib
"""
from stdlib.abstract.iterator import Iterator


class Node(object):
    def __init__(self):
        self.item = None
        self.next = type(self)


class Bag(object):
    """
    A data structure consisting of only the ability to add and iterate through the items.
    """
    class __ListIterator(Iterator):
        def __has_next__(self) -> bool:
            return type(self.__current.next).__name__ == 'Node'

        def __remove__(self):
            return NotImplementedError

        def __init__(self, first: Node):
            self.__current: Node = first

        def __next__(self):
            if not self.__has_next__():
                raise StopIteration

            item = self.__current.item
            self.__current = self.__current.next
            return item

    def __init__(self):
        self.__first: Node = Node()
        self.__n: int = 0

    def __iter__(self) -> __ListIterator:
        """
        Attempts to iterate through all the objects in the Bag
        :return: an iterator through the Bag
        :rtype: __ListIterator
        """
        return self.__ListIterator(self.__first)

    def __empty__(self) -> bool:
        """
        Checks if the Bag is empty
        :return: True if the Bag is empty, else it returns False
        :rtype: bool
        """
        return self.__first is None

    def __sizeof__(self) -> int:
        """

        :return:
        :rtype:
        """
        return self.__n

    def add(self, item: object) -> None:
        old_first = self.__first
        self.__first = Node()
        self.__first.item = item
        self.__first.next = old_first
        self.__n += 1

    def __add__(self, other: object) -> None:
        return self.add(other)


def main():
    bag = Bag()
    bag.add(1)
    bag.add(3)
    bag.add(5)
    for i in bag:
        print(i)


if __name__ == '__main__':
    main()
